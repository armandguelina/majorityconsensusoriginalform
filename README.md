# MajorityConsensus

This application computes election results using the Majority Consensus method.
It takes either a `.csv` file  followed by option 0 or a csvString with ; as delimeter followed by option 1 containing the sum of all the votes for each candidate as parameter, such as:

For a .csv file as parameter :

```csv
Candidates,Reject,Poor,Acceptable,Good,VeryGood,Excellent
Y,20,10,10,20,10,30
Z,20,40,10,10,15,5
X,20,20,30,10,15,5
A,15,15,15,15,15,25
K,30,20,10,10,15,5
```

For a csvString as parameter  :

```csv
"Candidates,Reject,Poor,Acceptable,Good,VeryGood,Excellent;Y,20,10,10,20,10,30;Z,20,40,10,10,15,5;X,20,20,30,10,15,5;A,15,15,15,15,15,25;K,30,20,10,10,15,5"
```

and return the results in a generated `.tsv` file or a stdout output depending on the option chosen. 0 for a .tsv file and 1 for a stdout output:

```tsv
Candidates	Grades	Rank
Z	75	1
K	72	2
X	-5	3
Y	-20	4
A	-25	5
```

It can be run directly:
For a .tsv file as an output :

```
java MajorityConsensus.java example/MC.csv 0
```
For a stdout output :

```
java MajorityConsensus.java "Candidates,Reject,Poor,Acceptable,Good,VeryGood,Excellent;Y,20,10,10,20,10,30;Z,20,40,10,10,15,5;X,20,20,30,10,15,5;A,15,15,15,15,15,25;K,30,20,10,10,15,5" 1
```

or be compiled to bytecode and run:
.tsv file output :

```
javac MajorityConsensus.java
java MajorityConsensus example/MC.csv 0
```

stdout output :

```
javac MajorityConsensus.java
java MajorityConsensus "Candidates,Reject,Poor,Acceptable,Good,VeryGood,Excellent;Y,20,10,10,20,10,30;Z,20,40,10,10,15,5;X,20,20,30,10,15,5;A,15,15,15,15,15,25;K,30,20,10,10,15,5" 1
```
