/*------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------IMPORTS---------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/

import java.io.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;  
import java.lang.Math;


/*------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------Class-----------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/

public class MajorityConsensus  
{  

	public static int countCpt= 0;
	//Fonction qui calcul les écarts types des notes des candidats
	public static String[][] formule(double[][] tab, String[][] tabString, String file) throws IOException 
	{
		//Initialisation des variables
		String[][] tabVal    = new String[countCpt][2];
		ArrayList<String> al = new ArrayList<String>();
		double mediane       = 50;
		double total         = 0 ;
		int    current       = 0 ;
		int    cpt           = 0 ;
		String strTotal      = "";
		
		al.add("add"); //Ajout pour ocuuper le première place
		for(int i=1; i< tab.length; i++)
		{
			//System.out.println(tab[i]);
			//Boucle permettant de dterminer l'indice correspond à la mediane poue chaque candidat
			for(int j=1; j<7; j++)
			{
				if(total >=mediane)
				{
					current  = j-1 ; 
					break;
				}
				else
				{
					total    = total + tab[i][j];
				}
			}
			 
			total = 0;
			//Boucle qui permet de calculer l'écart de chaque candidat par rapport à la médiane en utilisant la formule £Pi(Di--Dm)
			//Pi=pourcentage
			//Di=delta des valeurs des indexes
			//Dm=delta de la médiane
			for(int j=1; j<7; j++)
			{
				total = total + ((tab[i][j])*(j-current));
				/*System.out.println(tab[i][j]+ " " + "*( " + j + " -  "+  current + " )");
				System.out.println(total);
				System.out.println();
				System.out.println();*/
			}
			strTotal = total + "";
			al.add(strTotal);
			
			//Pour chaque candadidat, on inscrie son nom dans le tableau
			for(int k=0; k< tabVal.length; k++)
			{
				//System.out.println(tab[i]);
				for(int j=0; j<2; j++)
				{
					if(j==0)
					{
						tabVal[k][0] = tabString[k][0];
					}
					tabVal[0][1] = "Ecart type";
				}
			}
			
			total   = 0;
			current = 0;
			
		}
		
		//Ajout des valeurs des écarts types correspondant à chaque candidat dans le tableau
		for(int i =1; i< al.size(); i++)
		{
			tabVal[i][1] = al.get(i);
		}
		//System.out.println(printResultsTab(tabVal, file));
		return tabVal;
	}

	//Fonction qui permet de compter le nombre de lignes dqns un fichier .csv
	//Prends en paramètres le nom du fichier et renvoie une nombre de lignes
	public static int count(String filename) throws IOException 
	{
		InputStream is = new BufferedInputStream(new FileInputStream(filename)); //Ouvre le fichier
		try 
		{
			//Initiation des variables
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			
			//Verifie à chaque fois si le ligne courrante est vide ou pas en utilisant la valeur du premier caractère trouvé
			while ((readChars = is.read(c)) != -1) 
			{
				empty = false;
				for (int i = 0; i < readChars; i++)
				{
					//Lorsqu'on arrive au dernier caractère de la ligne, avant le saut de ligne, nous incrementons le compteur de +1
				    if (c[i] == '\n') 
				    {
				        count++;
				    }
				}
			}
			//Si le compteur où le compter est de 0 ou fichier vide renvoie 0
			//Sinon renvoie le nombre de ligne du fichier
			countCpt = count;
			return (count == 0 && !empty) ? 0 : count; 
		} 
		finally 
		{
			is.close();
	   }
	}
	
	
	//Fonction qui permet de compter le nombre de lignes dqns un fichier .csv
	//Prends en paramètres le csv sous forme de stdin séparé par des virgules pour les valeurs et \n à la fin de chaque ligne et renvoie une nombre de lignes
	public static int countTsvString(String name) throws IOException 
	{
		char someChar = ';';
		int count = 0;
		 
		for (int i = 0; i < name.length(); i++) {
			if (name.charAt(i) == someChar) {
				count++;
				//System.out.println("hello");
			}
		}
		return count;
	}
	
	
	//Fonction qui permet de lire un fichier .tsv et de mettre le comtenu du fichier dans un tableau
	//Prend en paramètre le nom du fichier et renvoie un tableau contenant les valeurs du fichier
	public static String[][] readCSV(String file) throws IOException 
	{
		//Initialisation des variables
		String[] tab         = new String[countCpt];
		String[][] tabVal    = new String[countCpt][7];
		String val           = "";
		int cpt              = 0;
		//Analyse d'un fichier CSV dans le constructeur de la classe Scanner 
		Scanner sc           = new Scanner(new File(file));  
		sc.useDelimiter("\n");   //Delimiter  
		while (sc.hasNext())  //verification 
		{  
			val = sc.next();
			tab[cpt] = val; //Enregistre les valeurs de la ligne dans un tableau
			cpt++;
		}   
		sc.close();  //Fermeture de l'analyseur  
		
		//Boucle qui recupère les valeur de chaque ligne et stock le tout dans un tableau à 2 dimensions
		for(int i=0; i< tab.length; i++)
		{
			String[] tmp = tab[i].split(",");
			for(int j=0; j<7; j++)
			{
				tabVal[i][j] = tmp[j];
			}
		}
		//System.out.println(print(tabVal, file)); //Visualisation avant renvoie
		return tabVal;

	}
	
	
	//Fonction qui permet de lire un csv dans stdin sous forme de String et de mettre le comtenu du fichier dans un tableau
	//Prend en paramètre le nom du fichier et renvoie un tableau contenant les valeurs du fichier
	public static String[][] readCSVString(String name) throws IOException 
	{
		//Initialisation des variables
		String[] tab         = new String[countCpt];
		String[][] tabVal    = new String[countCpt][7];
		String val           = "";
		int cpt              = 0;
		
		char someChar = ';';
		String line="";
		
		for (int i = 0; i < name.length(); i++) 
		{
			if (name.charAt(i) == someChar) 
			{
				tab[cpt] = line;
				//System.out.println(tab[cpt]);
				cpt++;
				line="";
			}
			else
			{
				line = line+name.charAt(i);
			}
		}
		
		//Boucle qui recupère les valeur de chaque ligne et stock le tout dans un tableau à 2 dimensions
		for(int i=0; i< tab.length; i++)
		{
			String[] tmp = tab[i].split(",");
			for(int j=0; j<7; j++)
			{
				tabVal[i][j] = tmp[j];
			}
		}
		//System.out.println(print(tabVal, file)); //Visualisation avant renvoie
		return tabVal;

	}   
	
	
	//Fonction qui permet d'effectuer un jugement consensuelle
	public static void MajorityConsensus(String file, String option) throws IOException 
	{
		//Initialisation des variables
		if(option.equals("0"))
		{
			countCpt = count(file);
		}
		if(option.equals("1"))
		{
			countCpt = countTsvString(file);
		}
		
		double[][] tabValDouble = new double[countCpt][7];
		String[][] tabVal       = new String[countCpt][7];
		String[][] tabValResults= new String[countCpt][2];
		String[][] tabValEcart  = new String[countCpt][2];
		
		if(option.equals("0"))
		{
			tabVal       = readCSV(file); //Lie le fichier .csv
		}
		if(option.equals("1"))
		{
			tabVal       = readCSVString(file); //Lie le csvString
		}
		
		tabValDouble = GetPercentages(tabVal, file); //Renvoie les pourcentages des votes
		/*System.out.println(printDouble(tabValDouble, file)); //Visualisation
		tabValResults= getResultsAccordingToMediane(tabValDouble, tabVal, file); //Resultat par rapport à la médiane*/
		tabValEcart = formule(tabValDouble, tabVal, file); //Ecrat type de chaque candidat
		//System.out.println(printResultsTab(tabValEcart, file));
		
		tabVal = order(tabVal, tabValEcart, file);//Affiche les resultat par order du 1er au dernier
		//System.out.println(printResultsTab(tabVal, file)); //Visualisation
		saveFile(tabVal); //Sauvegarde le tout dans un fichier
		
		if(option.equals("0"))
		{
			System.out.println("File containing results generated : Results.tsv");
		}
		if(option.equals("1"))
		{
			readTxtFile("Results.tsv");
		}
	}
	
	// function to sort hashmap by values
    public static HashMap<String, Integer> sortByValue(HashMap<String, Integer> hm)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<String, Integer> > list =
               new LinkedList<Map.Entry<String, Integer> >(hm.entrySet());
 
        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2)
            {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });
         
        // put data from sorted list to hashmap
        HashMap<String, Integer> temp = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }
 


	//Fonction qui classe les candidats par rapport aux ecarts
	public static String[][] order(String[][] tabString, String[][] tab, String file) throws IOException 
	{
	
		//Initialisation des variables
		HashMap<String, Integer> sort = new HashMap<String, Integer>() ;
		String[][] tabVal             = new String[countCpt][2];
		
		//Remplit l'hashmap en mmettant candidats et valeur final de l'écart
		for(int i=1; i< tab.length; i++)
		{	
			sort.put(tabString[i][0], (int) Double.parseDouble(tab[i][1]));			
		}
		
		sort = sortByValue(sort); //Ordonne de manière decroissante
		//System.out.println(sort);
		
		//Nous mettons les valeur dans le tableau qui sera renvoyé après
		int cpt =1;
		for (Map.Entry<String, Integer> set :sort.entrySet()) 
		{
			if(cpt< tabVal.length)
			{
				tabVal[cpt][0]=set.getKey();
				
			}	
			cpt++;
		}
		
		cpt =1;
		for (Map.Entry<String, Integer> set :sort.entrySet()) 
		{
			if(cpt< tabVal.length)
			{
				tabVal[cpt][1]=set.getValue() + "";
			}	
			cpt++;
		}
		tabVal[0][0] = "Candidates";
		tabVal[0][1] = "Grades";
		
		return tabVal;
	}
	
	
	//Fonction qui permet d'obetnir les résultats des candidats
	public static String[][] getResultsAccordingToMediane(double[][] tab, String[][] tabString, String file) throws IOException 
	{
		//Initialisation des variables
		String[][] tabVal    = new String[countCpt][2];
		ArrayList<String> al = new ArrayList<String>();
		double mediane       = 50;
		double total         = 0 ;
		int    current       = 0 ;
		
		al.add("add"); //Ajout pour ocuuper le première place
		
		//On determine l'indice de la mediane pour chaque candidat
		for(int i=1; i< tab.length; i++)
		{
			//System.out.println(tab[i]);
			for(int j=1; j<7; j++)
			{
				if(total <mediane)
				{
					total    = total + tab[i][j];
					current  = j ; 
				}
				else
				{
					break;
				}
			}
			//System.out.println(total + " " + current);
			al.add(tabString[0][current]); //Ajout de la note dans le tableau de notes
			
			//Pour chaque candadidat, on inscrie son nom dans le tableau
			for(int k=0; k< tabVal.length; k++)
			{
				//System.out.println(tab[i]);
				for(int j=0; j<2; j++)
				{
					if(j==0)
					{
						tabVal[k][0] = tabString[k][0];
					}
					tabVal[0][1] = "grade";
				}
			}
			
			total   = 0;
			current = 0;
		}
		
		//Ajout des valeurs des notes correspondant à chaque candidat dans le tableau
		for(int i =1; i< al.size(); i++)
		{
			tabVal[i][1] = al.get(i);
		}
		//System.out.println(printResultsTab(tabVal, file));
		return tabVal;
	}
	
	//Fonction qui permet d'obtenir les pourcentages correspondant aux notes des candidats
	public static double[][] GetPercentages(String[][] tab, String file) throws IOException 
	{
		//Initialisation des variables
		double[][] tabVal = new double[countCpt][7];
		double total      = 0;
		
		//Boucle qui pour chaque note donné à un candidat donné calcul son pourcentage
		for(int i=1; i< tab.length; i++)
		{
			//System.out.println(tab[i]);
			for(int j=1; j<7; j++)
			{
				total = total + Integer.parseInt(tab[i][j]);
			}
			//System.out.println(total);
			
			for(int j=1; j<7; j++)
			{
				double percent = (Double.parseDouble(tab[i][j]) / (double) total)*100;
				tabVal[i][j] = Math.round(percent * 100.0) / 100.0;
				//System.out.println(tabVal[i][j]);
			}
			total = 0;
		}
		return tabVal;
	}
	
	//Fontion permettant de sauvegarder le résultat du tout dans un fichier
	public static void saveFile(String[][] tab) throws IOException 
	{
		 // Creating an instance of file
        Path path = Paths.get("Results.tsv");

        // Custom string as an input
        String str = "Candidates\tGrades\tRank\n";
            

		for ( int i = 1; i < tab.length; i++ )
		{
			for ( int j = 0; j < 2; j++ )
			{
				if(j ==1)
				{
					str = str + tab[i][j] + "\t" + i + "\n";
				}
				else
				{
					str = str + tab[i][j] + "\t";
				}
			}
		}
		
        Files.writeString(path, str, StandardCharsets.UTF_8);
	}
	
	public static void readTxtFile(String file) throws IOException 
	{
        File file1 = new File(file);
        BufferedReader br = new BufferedReader(new FileReader(file1));
        String st;
        while ((st = br.readLine()) != null)
        {
            System.out.println(st);
      	}
    }
	
	
	/*------------------------------------------------------------------------------------------------------------------------*/
	/*--------------------------------------------FONCTIONS POUR AFFICHAGES---------------------------------------------------*/
	/*------------------------------------------------------------------------------------------------------------------------*/
	
	public static String printResultsTab(String[][] tab, String file) throws IOException 
	{
		String str = "";
		for( int k =0; k< 2; k++)
		{
			str= str + "+---" ;
		}
		str = str + "\n" ;

		for ( int i = 0; i < countCpt; i++ )
		{
			for ( int j = 0; j < 2; j++ )
			{
				str = str + "|";
				str = str + " " + tab[i][j] + " ";
			}

			str = str + "|\n";

			for( int l =0; l< 2; l++)
			{
				str = str + "+---" ;
			}
			str = str + "\n" ;
		}
		return str;
	}
	
	
	public static String print(String[][] tab, String file) throws IOException 
	{
		String str = "";
		for( int k =0; k< 7; k++)
		{
			str= str + "+---" ;
		}
		str = str + "\n" ;

		for ( int i = 0; i < countCpt; i++ )
		{
			for ( int j = 0; j < 7; j++ )
			{
				str = str + "|";
				str = str + " " + tab[i][j] + " ";
			}

			str = str + "|\n";

			for( int l =0; l< 7; l++)
			{
				str = str + "+---" ;
			}
			str = str + "\n" ;
		}
		return str;
	}
	
	public static String printDouble(double[][] tab, String file) throws IOException 
	{
		String str = "";
		for( int k =0; k< 7; k++)
		{
			str= str + "+---" ;
		}
		str = str + "\n" ;

		for ( int i = 0; i < countCpt; i++ )
		{
			for ( int j = 0; j < 7; j++ )
			{
				str = str + "|";
				str = str + " " + tab[i][j] + " ";
			}

			str = str + "|\n";

			for( int l =0; l< 7; l++)
			{
				str = str + "+---" ;
			}
			str = str + "\n" ;
		}
		return str;
	}
	
	
	/*------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------Main-----------------------------------------------------------*/
	/*------------------------------------------------------------------------------------------------------------------------*/
	
	public static void main(String[] args) throws Exception  
	{  
		MajorityConsensus(args[0], args[1]);
	}
	

}  
